import type { PositionType } from "@/models";

export const getPosition = (element: HTMLElement): PositionType => {
  const rect = element.getBoundingClientRect();

  const center = rect.height / 2 - 20;
  const top = rect.top + center;
  const right = rect.left + rect.width;
  const left = rect.left;

  return { top: top, right: right, left: left };
};

export const getCurvePath = (
  startX: number,
  startY: number,
  endX: number,
  endY: number
): string => {
  const AX = startX;
  const AY = startY;

  const BX = Math.abs(endX - startX) * 0.05 + startX;
  const BY = startY;

  const CX = (endX - startX) * 0.66 + startX;
  const CY = startY;
  const DX = (endX - startX) * 0.33 + startX;
  const DY = endY;
  const EX = -Math.abs(endX - startX) * 0.05 + endX;
  const EY = endY;

  const FX = endX;
  const FY = endY;

  let path = "M" + AX + "," + AY;
  path += " L" + BX + "," + BY;
  path += " " + "C" + CX + "," + CY;
  path += " " + DX + "," + DY;
  path += " " + EX + "," + EY;
  path += " L" + FX + "," + FY;

  return path;
};

export const getCoords = (element: HTMLElement) => {
  const box = element.getBoundingClientRect();

  const body = document.body;
  const docEl = document.documentElement;

  const scrollTop = docEl.scrollTop || body.scrollTop;
  const scrollLeft = docEl.scrollLeft || body.scrollLeft;

  const clientTop = docEl.clientTop || body.clientTop || 0;
  const clientLeft = docEl.clientLeft || body.clientLeft || 0;

  const top = box.top + scrollTop - clientTop;
  const left = box.left + scrollLeft - clientLeft;

  return { top: Math.round(top), left: Math.round(left) };
};
