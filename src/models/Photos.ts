export type PhotosDataType = {
  activeCardIdx: number | null;
};

export type PhotoType = {
  albumId: number;
  id: number;
  thumbnailUrl: string;
  title: string;
  url: string;
};

export type PhotoSelectEmitType = {
  el: HTMLAnchorElement;
  photo: PhotoType;
};
