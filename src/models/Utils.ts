export type PositionType = {
  left: number;
  top: number;
  right: number;
};
