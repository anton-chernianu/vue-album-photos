import type { PhotoSelectEmitType } from "./Photos";

export type AlbumType = {
  id: number;
  userId: number;
  title: string;
  el: HTMLAnchorElement | null;
  photos: PhotoSelectEmitType[];
};
