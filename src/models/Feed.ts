import type { PhotoSelectEmitType, AlbumType, PhotoType } from "@/models";

export type FeedDataType = {
  containerOffset: {
    left: number;
    top: number;
  } | null;
  candidatePhoto: PhotoSelectEmitType | null;
  albums: AlbumType[];
  photos: PhotoType[];
};
