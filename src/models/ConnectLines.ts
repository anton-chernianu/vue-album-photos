export type ConnectLineType = {
  from: HTMLElement;
  to: HTMLElement;
};
